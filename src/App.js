import avatar from "./assets/images/48.jpg";
import style from "./App.module.css";

function App() {
  return (
    <div className={style.devcampContainer}>
      <img src={avatar} class={style.devcampAvatar} />
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      <p class={style.devcampIntro}><span class={style.devcampName}>Tammy Stevens</span> . Front End Developer</p>
    </div>
  );
}

export default App;
